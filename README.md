# Dynare-Jupyter

A Docker image containing:
* [DBnomics](https://db.nomics.world/)
* [Dynare](http://www.dynare.org/)
* [Jupyter](https://jupyter.org/)
* [Pandas](https://pandas.pydata.org/)
* [R](https://www.r-project.org/)
