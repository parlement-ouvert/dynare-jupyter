# Dynare-Jupyter

## Installation on Debian Testing

### JupyterLab Installation

Cf:
* http://jupyter.org/install
* http://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html

As user:
```bash
python3 -m pip install jupyter
python3 -m pip install jupyterlab
```

Start JupyterLab using:
```bash
jupyter lab
```

### Dynare Installation

Cf https://github.com/DynareTeam/dynare/tree/master

As `root`:
```bash
apt install octave

# To compile Dynare:
apt build-dep dynare

```

The new system build of the preprocessor is not yet official.

In the mean time, you must use development version of Dynare => As user:
```bash
git clone --recursive https://github.com/DynareTeam/dynare.git
cd dynare
autoreconf -si
./configure --disable-matlab
    Dynare is now configured for building the following components...

    Binaries (with "make"):
        MEX files for Octave (except those listed below):                   yes
        MS-SBVAR MEX files for Octave:                                      yes
        Kalman Steady State MEX file for Octave:                            yes
        K-order and dynare_simul MEX for Octave:                            yes
        Linsolve for Octave:                                                no (Octave >= 3.8)
        Ordschur for Octave:                                                no (Octave >= 4.0)
make
sudo make install
cd ..
```

## Docker Image Generation

```bash
docker build -t cepremap/dynare-jupyter .
# Or, alternatively, to force rebuild:
docker build --no-cache -t cepremap/dynare-jupyter .

docker tag cepremap/dynare-jupyter cepremap/dynare-jupyter:v0.1.4

docker login
    Username: eraviart
    Password: XXXX
    Login Succeeded

docker push cepremap/dynare-jupyter:v0.1.4
```
