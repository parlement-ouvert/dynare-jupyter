FROM debian:testing
LABEL maintainer="dynaresf@parlement-ouvert.fr"

# Install Dynare.
RUN echo "deb-src http://deb.debian.org/debian testing main" >>/etc/apt/sources.list
RUN echo "deb-src http://security.debian.org testing/updates main" >>/etc/apt/sources.list
RUN apt-get update
RUN apt-get build-dep --no-install-recommends -y dynare
RUN apt-get install -y bison ca-certificates flex gawk git libfl-dev
RUN git clone --recursive https://github.com/DynareTeam/dynare.git
WORKDIR "/dynare"
RUN autoreconf -si
RUN ./configure --disable-matlab
RUN make
RUN make install
WORKDIR "/"

# Install JupyterLab, Pandas, DBnomics Python Client.
RUN apt-get install -y python3-pip
RUN python3 -m pip install --no-cache-dir jupyterlab pandas
RUN python3 -m pip install --no-cache-dir git+https://git.nomics.world/dbnomics/dbnomics-python-client.git

# Install R, IRKernel, DBnomics R client.
RUN apt-get install -y libcurl4-openssl-dev libssl-dev libxml2-dev libzmq3-dev r-base
RUN R -e "install.packages(c('crayon', 'devtools', 'pbdZMQ'))"
RUN R -e "devtools::install_github(paste0('IRkernel/', c('repr', 'IRdisplay', 'IRkernel')))"
RUN R -e "IRkernel::installspec(user = FALSE)"
RUN R -e "install.packages('tidyverse')"
RUN R -e "install.packages('R.matlab')"
RUN R -e "devtools::install_github('dbnomics/rdbnomics')"

# Install Octave libraries.
RUN octave --eval "pkg install -forge -global io" --no-window-system